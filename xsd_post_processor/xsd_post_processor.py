from itertools import islice
import logging
import sys
from importlib.metadata import version
from io import BufferedReader
from logging.config import dictConfig
from pathlib import Path
from typing import cast

import click
from lxml import etree
from lxml.etree import _Element
from yaml import CLoader
from yaml import load


def configure_logging(log_level: int) -> None:
    config_file = str(
        Path(getattr(sys, "_MEIPASS", Path(__file__).parent))
        / Path("resources")
        / Path("logging-config.yml")
    )
    with open(config_file, "rt") as f:
        config = load(f, Loader=CLoader)
        config["handlers"]["console"]["level"] = log_level
        dictConfig(config)
    return


def get_log_level(verbose: bool) -> int:
    if verbose:
        return logging.DEBUG
    else:
        return logging.INFO


scl_namespaces = {
    "xs": "http://www.w3.org/2001/XMLSchema",
    "scl": "http://www.iec.ch/61850/2003/SCL",
}


def add_schema_location_attribute(import_element):
    import_element.attrib["schemaLocation"] = "SCL.xsd"


def remove_duplicate_scl_imports(tree):
    imports = islice(tree.findall("xs:import", namespaces=scl_namespaces), 1, None)
    for import_element in imports:
        import_element.getparent().remove(import_element)


def fix_tp_addr(tp_addr: _Element) -> None:
    logger = logging.getLogger(__name__)
    logger.info("Locating the tP_ADDR->Type element...")
    tp_addr_type_attribute = cast(
        list[_Element],
        tp_addr.xpath(
            "xs:complexContent/xs:restriction/xs:attribute", namespaces=scl_namespaces
        ),
    )[0]
    logger.debug(
        "tP_ADDR->Type Attribute: \n%s", etree.tostring(tp_addr_type_attribute)
    )
    logger.info("Locating the tP_ADDR->complexContent element...")
    tp_addr_complex_content = cast(
        list[_Element],
        tp_addr.xpath(
            "xs:complexContent",
            namespaces=scl_namespaces,
        ),
    )[0]
    logger.debug(
        "tP_ADDR->complexContent: \n%s", etree.tostring(tp_addr_complex_content)
    )
    logger.info("Removing the tP_ADDR->complexContent element...")
    tp_addr.remove(tp_addr_complex_content)
    logger.debug("tP_ADDR: \n%s", etree.tostring(tp_addr))
    logger.info("Adding the simpleContent element...")
    tp_addr_simple_content = etree.SubElement(
        tp_addr, f"{{{scl_namespaces['xs']}}}simpleContent"
    )
    logger.debug("tP_ADDR: \n%s", etree.tostring(tp_addr))
    logger.info("Adding the restriction element...")
    tp_addr_simple_content_restriction = etree.SubElement(
        tp_addr_simple_content,
        f"{{{scl_namespaces['xs']}}}restriction",
        attrib={"base": "scl:tP"},
    )
    logger.debug("tP_ADDR: \n%s", etree.tostring(tp_addr))
    patterns = [
        "0",
        "[1-9][0-9]{0,3}",
        "[1-5][0-9]{4,4}",
        "6[0-4][0-9]{3,3}",
        "65[0-4][0-9]{2,2}",
        "655[0-2][0-9]",
        "6553[0-5]",
    ]
    logger.info("Adding the patterns...")
    for pattern in patterns:
        tp_addr_simple_content_restriction.append(
            etree.Element(
                f"{{{scl_namespaces['xs']}}}pattern",
                attrib={"value": pattern},
                nsmap=scl_namespaces,
            )
        )
    logger.debug("tP_ADDR: \n%s", etree.tostring(tp_addr))
    logger.info("Adding the Type Attribute...")
    tp_addr_simple_content_restriction.append(tp_addr_type_attribute)
    logger.debug("tP_ADDR: \n%s", etree.tostring(tp_addr, pretty_print=True))


def fix_tp_port(tp_port: _Element) -> None:
    logger = logging.getLogger(__name__)
    logger.info("Locating the tP_ADDR->Type element...")
    tp_port_type_attribute = cast(
        list[_Element],
        tp_port.xpath(
            "xs:complexContent/xs:restriction/xs:attribute", namespaces=scl_namespaces
        ),
    )[0]
    logger.debug(
        "tP_PORT->Type Attribute: \n%s", etree.tostring(tp_port_type_attribute)
    )
    logger.info("Locating the tP_ADDR->complexContent element...")
    tp_port_complex_content = cast(
        list[_Element],
        tp_port.xpath(
            "xs:complexContent",
            namespaces=scl_namespaces,
        ),
    )[0]
    logger.debug(
        "tP_PORT->complexContent: \n%s", etree.tostring(tp_port_complex_content)
    )
    logger.info("Removing the tP_PORT->complexContent element...")
    tp_port.remove(tp_port_complex_content)
    logger.debug("tP_PORT: \n%s", etree.tostring(tp_port))
    logger.info("Adding the simpleContent element...")
    tp_port_simple_content = etree.SubElement(
        tp_port, f"{{{scl_namespaces['xs']}}}simpleContent"
    )
    logger.debug("tP_PORT: \n%s", etree.tostring(tp_port))
    logger.info("Adding the restriction element...")
    tp_port_simple_content_restriction = etree.SubElement(
        tp_port_simple_content,
        f"{{{scl_namespaces['xs']}}}restriction",
        attrib={"base": "scl:tP"},
    )
    logger.debug("tP_PORT: \n%s", etree.tostring(tp_port))
    patterns = [
        "0",
        "[1-9][0-9]{0,3}",
        "[1-5][0-9]{4,4}",
        "6[0-4][0-9]{3,3}",
        "65[0-4][0-9]{2,2}",
        "655[0-2][0-9]",
        "6553[0-5]",
    ]
    logger.info("Adding the patterns...")
    for pattern in patterns:
        tp_port_simple_content_restriction.append(
            etree.Element(
                f"{{{scl_namespaces['xs']}}}pattern",
                attrib={"value": pattern},
                nsmap=scl_namespaces,
            )
        )
    logger.debug("tP_PORT: \n%s", etree.tostring(tp_port))
    logger.info("Adding the Restriction Element...")
    tp_port_simple_content_restriction.append(tp_port_type_attribute)
    logger.debug("tP_PORT: \n%s", etree.tostring(tp_port, pretty_print=True))


@click.command()
@click.version_option(
    version=version("xsd-post-processor"), prog_name="iec61850-80-5-xsd-post-processor"
)
@click.option(
    "--debug/--no-debug",
    default=False,
    help="Use this flag to enable more verbose logging to the console.",
)
@click.option("-w", "--write-to-file", type=click.File(mode="wb"))
@click.argument("xsd_file", type=click.File(mode="rb"))
def xsd_post_processor(
    xsd_file: BufferedReader, write_to_file: click.utils.LazyFile, debug: bool
) -> None:
    """Fix the XSD output generated by EA.

    XSD_FILE is the path to the XSD file generated by EA that needs to be fixed
    """
    logger = logging.getLogger(__name__)
    log_level = get_log_level(debug)
    configure_logging(log_level)
    logger.info("Reading xsd file: %s...", xsd_file.name)
    tree = etree.parse(xsd_file)
    logger.info("Locating the tP_ADDR element...")
    tp_addr = cast(
        list[_Element],
        tree.xpath(
            "/xs:schema/xs:complexType[@name='tP_ADDR']",
            namespaces=scl_namespaces,
        ),
    )[0]
    logger.debug("tP_ADDR: \n%s", etree.tostring(tp_addr))
    fix_tp_addr(tp_addr)
    logger.info("Locating the tP_PORT element...")
    tp_port = cast(
        list[_Element],
        tree.xpath(
            "/xs:schema/xs:complexType[@name='tP_PORT']",
            namespaces=scl_namespaces,
        ),
    )[0]
    logger.debug("tP_PORT: \n%s", etree.tostring(tp_port))
    fix_tp_port(tp_port)
    logger.info("Locating the tP_IPModbus element...")
    tp_ipmodbus = cast(
        list[_Element],
        tree.xpath(
            "/xs:schema/xs:complexType[@name='tP_IPModbus']",
            namespaces=scl_namespaces,
        ),
    )[0]
    logger.debug("tP_IPModbus: \n%s", etree.tostring(tp_ipmodbus))
    logger.debug("Removing duplicate xsi:import elements pointing to the SCL namesapce")
    remove_duplicate_scl_imports(tree)
    logger.debug("Adding schemaLocation attribute to the xs:import element")
    add_schema_location_attribute(tree.xpath("xs:import", namespaces=scl_namespaces)[0])
    logger.info("Writing the updated xsd file...")
    etree.indent(tree, space="\t")
    if write_to_file:
        tree.write(
            write_to_file, pretty_print=True, xml_declaration=True, encoding="UTF-8"
        )
    else:
        print(etree.tostring(tree, pretty_print=True).decode("UTF-8"))


if __name__ == "__main__":  # pragma: no cover
    xsd_post_processor()
