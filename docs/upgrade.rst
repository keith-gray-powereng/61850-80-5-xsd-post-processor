=======
Upgrade
=======

Stable release
--------------

Since the application is distributed as a single executable file,
an upgrade consists of downloading the latest 
iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe release from 
https://gitlab.com/keith-gray-powereng/61850-80-5-xsd-post-processor/-/releases 
and running it instead of the older version.
