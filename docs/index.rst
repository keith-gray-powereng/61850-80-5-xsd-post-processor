.. IEC 61850-80-5 XSD Post Processor documentation master file, created by
   sphinx-quickstart on Fri Jul  2 08:15:46 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IEC 61850-80-5 XSD Post Processor's documentation!
=============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   upgrade
   authors
   changelog
   contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
