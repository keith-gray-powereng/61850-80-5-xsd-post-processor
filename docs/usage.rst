.. _section-heading-usage:

=====
Usage
=====

IEC 61850-80-5 XSD Post Processor is a command line utility that takes an \*.xsd
file exported from the UML model in Enterprise Architect and modifies it to address
some deficiencies in the generated UML model.

.. note::
   The commands shown below assume the iec61850-80-5-xsd-post-processor
   executable file is located in the current working directory.
   If that is not the case, you will need to either change the
   current working directory using the *cd* command or provide
   the full path to the executable as seen in the example below.

   .. code-block:: console

      c:\> c:\users\kgray\downloads\iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe --help

Help
====

The command help can be shown using the help flag.

.. code-block:: console

   c:\> iec61850-xsd-post-processor-yyyy.mm.dd.exe --help
   Usage: iec61850-80-5-xsd-post-processor-c8e7375d.exe [OPTIONS] XSD_FILE

   Fix the XSD output generated by EA.

   XSD_FILE is the path to the XSD file generated by EA that needs to be fixed

   Options:
   --help  Show this message and exit.

Version Information
===================

The version information can be displayed on the console through the use
of the --version flag.

.. code-block:: batch

    c:\> iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe --version
    iec61850-80-5-xsd-post-processor, version yyyy.mm.dd

Execution
=========

The following code shows an example of how to use this application. It assumes the XSD file is in the same
directory as the executable.

.. code-block:: batch

    c:\> iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe modbus.xsd

Additional logging information can be seen in the console by adding the `--debug` flag as shown below.

.. code-block:: batch

    c:\> iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe --debug modbus.xsd

If the `-w, --write-to-file` option is not provided on the command line, the generated
|xsd| content is printed to the console. This is useful if you wish to further process
the content in another command line tool which can take content on stdin.

The `-w, --write-to-file` option will write the generated |xsd| content to the file
provided with the option. The following example shows how to use the `-w, --write-to-file`
option. In this example, the application will write the updated |xsd| content to the
`modbus-post.xsd` file in the current directory. The full path to the |xsd| file
can also be provided if you wish to write the file into a different directory.

.. code-block:: batch

    c:\> iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe --debug -w modbux-post.xsd modbus.xsd
