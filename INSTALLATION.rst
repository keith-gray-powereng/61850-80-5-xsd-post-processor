============
Installation
============

Stable release
--------------

The application is distributed as a single executable file.
Download the latest iec61850-80-5-xsd-post-processor-yyyy.mm.dd.exe release from 
https://gitlab.com/keith-gray-powereng/61850-80-5-xsd-post-processor/-/releases.

Open a command prompt and run the downloaded executable.
See the :ref:`section-heading-usage` section for more details on using the application.
