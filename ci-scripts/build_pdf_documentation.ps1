Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Building HTML Documentation"
conda run --no-capture-output -n $environment_name sphinx-build -M latex docs docs\_build
conda run --no-capture-output -n $environment_name pdflatex -aux-directory=docs/_build/latex -output-directory=docs/_build/latex "docs/_build/latex/iec61850-80-5xsdpostprocessor.tex"
conda run --no-capture-output -n $environment_name pdflatex -aux-directory=docs/_build/latex -output-directory=docs/_build/latex "docs/_build/latex/iec61850-80-5xsdpostprocessor.tex"
if (!$?) { Exit $LASTEXITCODE }
