Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Running flake8"
conda run --no-capture-output -n $environment_name flake8 --format=pylint --output-file pyflakes.log --tee xsd_post_processor 
if (!$?) { Exit $LASTEXITCODE }
