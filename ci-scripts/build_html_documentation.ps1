Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Building HTML Documentation"
conda run --no-capture-output -n $environment_name sphinx-build -M html docs docs\_build
Write-Host "Zipping the html docs directory"
Compress-Archive -path "docs\_build\html" "docs\_build\html.zip" -compressionlevel optimal
if (!$?) { Exit $LASTEXITCODE }
