Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Running black"
conda run --no-capture-output -n $environment_name black --quiet --check xsd_post_processor 
if (!$?) { Exit $LASTEXITCODE }
