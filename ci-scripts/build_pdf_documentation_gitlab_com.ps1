Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Building HTML Documentation"
& 'C:\Program Files\MiKTeX\miktex\bin\x64\pdflatex' -aux-directory=docs/_build/latex -output-directory=docs/_build/latex "docs/_build/latex/iec61850-80-5xsdpostprocessor.tex"
& 'C:\Program Files\MiKTeX\miktex\bin\x64\pdflatex' -aux-directory=docs/_build/latex -output-directory=docs/_build/latex "docs/_build/latex/iec61850-80-5xsdpostprocessor.tex"
if (!$?) { Exit $LASTEXITCODE }
