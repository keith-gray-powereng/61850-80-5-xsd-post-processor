Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Building executable"
conda run --no-capture-output -n $environment_name pyinstaller --onefile --name iec61850-80-5-xsd-post-processor-$CI_COMMIT_TAG --copy-metadata xsd_post_processor --add-data "xsd_post_processor\resources;resources" xsd_post_processor\xsd_post_processor.py 
if (!$?) { Exit $LASTEXITCODE }
