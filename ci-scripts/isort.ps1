Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Running isort"
conda run --no-capture-output -n $environment_name isort --check xsd_post_processor 
if (!$?) { Exit $LASTEXITCODE }

