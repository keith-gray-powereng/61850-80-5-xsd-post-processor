Set-PSDebug -Trace 1
Write-Host "Environment Name: $environment_name"
Write-Host "Running coverage"
conda run --no-capture-output -n $environment_name python -m pytest --junitxml=junit.xml --cov-report=html --cov=xsd_post_processor --cov-report=term-missing --cov-branch tests
if (!$?) { Exit $LASTEXITCODE }

