Set-PSDebug -Trace 1
Write-Host "Creating environment_name variable"
Write-Host "Environment Name: $environment_name"
Write-Host "Initializing Powershell"
conda init
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Creating environment $environment_name"
conda create -y -q --force -n $environment_name python=3.9
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Listing Environments"
conda env list
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Running python -V in $environment_name"
conda run --no-capture-output -n $environment_name python -V
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Installing Poetry"
conda run --no-capture-output -n $environment_name pip install poetry
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Poetry Version:"
conda run --no-capture-output -n $environment_name poetry --version
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Running poetry install"
conda run --no-capture-output -n $environment_name poetry install --ansi 
if (!$?) { Exit $LASTEXITCODE }